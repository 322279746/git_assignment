#include "AVLTree.h"
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "printTreeToFile.h"
using namespace std;


int main()
{

	BSNode<double>* avl = new AVLTree<double>(25);

	try
	{

		string textTree = "BSTData.txt";
		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(1);

		printTreeToFile(avl, textTree);
		system("pause");
		avl = avl->insert(2);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(16);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(14);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(6);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(19);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(32);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(23);

		printTreeToFile(avl, textTree);
		system("pause");

		avl = avl->insert(23);

	}
	catch (invalid_argument& e)
	{
		cout << "EXCEPTION: " << e.what() << endl;
		system("pause");
	}
	
	delete avl;
	return 0;
}

