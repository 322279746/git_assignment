#pragma once

#include <exception>
#include <iostream>
#include <string>
#include <fstream>
#include "BSNode.h"
using namespace std;


template<class T>
class AVLTree : public BSNode<T>
{
public:
	AVLTree(T data);


	BSNode<T>* insert(T value);
	
private:
	// returns the balance factor of the node
	int getBalanceFactor() const; 


	// make right rotation on the node
	AVLTree<T>* rotateRight();

	// make left rotation on the node
	AVLTree<T>* rotateLeft();


};

template<class T>
AVLTree<T>::AVLTree(T data) : BSNode<T>(data)
{

}


// override the base class function
template<class T>
BSNode<T>* AVLTree<T>::insert(T value)
{
	if (value == _data)
	{
		throw invalid_argument("Value already exist in tree");
	}

	if (value > _data)
	{

		if (this->_right)
		{
			// insert the node 
			_right = this->_right->insert(value);

			AVLTree* t = (AVLTree<T>*)_right;
			int balance = this->getBalanceFactor();
			int rightBalance = t->getBalanceFactor();

			// check if there is violation on this specific node becuase the insertion
			if (balance == -2)
			{
				// check if it is LR case or RR case

				// RL Case
				if (rightBalance == 1)
				{
					_right = t->rotateRight();
					return rotateLeft();
				}
				else if (rightBalance == -1) // RR Case
				{
					return rotateLeft();
				}
			}
		}
		else
		{
			_right = new AVLTree<T>(value);
		}
	}
	else
	{
		if (_left)
		{
			_left = this->_left->insert(value);
			
			AVLTree* t = (AVLTree<T>*)_left;
			int balance = this->getBalanceFactor();
			int leftBalance = t->getBalanceFactor();

			if (balance == 2)
			{
				// LR Case
				if (leftBalance == -1)
				{
					_left = t->rotateLeft();
					return rotateRight();	
				}
				else if (leftBalance == 1) // LL Case
				{
					return rotateRight();
				}	
			}
		}
		else
		{
			_left = new AVLTree<T>(value);
		}


	}

	return this;
}


template<class T>
int AVLTree<T>::getBalanceFactor() const
{
	int r = 0;
	int l = 0;

	if (this->getLeft())
	{
		l = this->getLeft()->getHeight();
	}

	if (this->getRight())
	{
		r = this->getRight()->getHeight();
	}


	return  l - r;

}


// make right rotation on the specific node!
template<class T>
AVLTree<T>* AVLTree<T>::rotateRight()
{
	AVLTree<T>* temp = (AVLTree<T>*)_left;

	this->_left = _left->getRight();
	temp->_right = this;

	return temp;
}

// make left rotation on the specific node!
template<class T>
AVLTree<T>* AVLTree<T>::rotateLeft()
{
	AVLTree<T>* temp = (AVLTree<T>*)_right;

	this->_right = _right->getLeft();
	temp->_left = this;

	return temp;
}