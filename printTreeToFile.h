#pragma once

#include <string>
#include "AVLTree.h"
//#include "RonAVL.h"
#include <fstream>

using namespace std;


template <class T>
void printData(BSNode<T>* node, ofstream& myfile)
{
	if (node == NULL)
		myfile << "# ";
	else
	{
		myfile << node->getData() << " ";   // change to the name of function that return the data
		printData(node->getLeft(), myfile); // change to the name of function that return the left node
		printData(node->getRight(), myfile);// change to the name of function that return the right node
	}
}

template <class T>
void printTreeToFile(BSNode<T>* bs, string output)
{
	ofstream myfile;
	myfile.open(output, ios::out);

	printData(bs, myfile);

	myfile.close();
}