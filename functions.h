#pragma once
#include <algorithm>

template <class T>
int compare(T a, T b)
{
	if (a > b)
		return -1;
	else if (b > a)
		return 1;
	else
		return 0;
}


template <class T>
void bubbleSort(T* a, int size)
{
	bool swapped = false;
	int iter = 0;
	do
	{
		swapped = false;
		for (int i = 1; i < size - iter; i++)
		{
			if (a[i - 1] > a[i])
			{
				std::swap(a[i - 1], a[i]);
				swapped = true;
			}
		}
		iter++;
	} while (swapped);
}


template <class T>
void printArray(T* arr, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << arr[i] << endl;
	}
}